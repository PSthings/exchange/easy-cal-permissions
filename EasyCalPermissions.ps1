﻿# Connect to ExchangeOnlineManagement. (Modules needs to be pre-installed)
Connect-ExchangeOnline

#Welcome message
Write-Host "`n`n##Welcome to Easy Calendar Permissions## `n`nPlease enter calendar Owner and Requestor AD username and select an action from options listed.`n(Enter the option # only)`n`n" -Foregroundcolor Green
# Get Owner and Requestors names
$owner=Read-Host 'Enter Calendar OWNER AD username'
$requester=Read-Host 'Enter REQUESTER AD username'

$Repeat = $True
While ($Repeat)
{
do {
    Write-Host "`n[1] Check existing calendar permissions of $owner" -foregroundcolor yellow
    Write-Host "`n[2] Assign Delegate permissions to $requester (Notifications: ON. $reuester will calendar notifications)" -foregroundcolor yellow
    Write-Host "`n[3] Assign Editor permissions to $requester (Notifications: OFF)" -foregroundcolor yellow
    Write-Host "`n[4] Remove calendar permissions of $requester from $($owner)" -foregroundcolor yellow

    $option  = read-host "`nPlease enter the option # you want to run"
    $confirm = read-host "`nYou selected number '$option', is that correct? [Y/N]"
    $check   = $null

    switch ($confirm) {
        Y       { $check = $true}
        N       { write-host "No problem, please try again"; start-sleep -seconds 2}
        default { write-warning "Invalid input, please try again"; start-sleep -seconds 2}
    }
    
}
until($check)

switch($option){
    1 {Get-MailboxFolderPermission -Identity "$owner$(':\calendar')" | Out-Host 
       [void](Read-Host 'Press Enter to go back to main Menu')}

    2 {Add-MailboxFolderPermission -Identity "$owner$(':\calendar')" -User $requester -AccessRights Editor -SharingPermissionFlags Delegate -confirm | Out-Host
       [void](Read-Host 'Press Enter to go back to main Menu')}


    3 {Add-MailboxFolderPermission -Identity "$owner$(':\calendar')" -User $requester -AccessRights Editor -SharingPermissionFlags None -confirm
       [void](Read-Host 'Press Enter to go back to main Menu')}


    4 {Remove-MailboxFolderPermission -Identity "$owner$(':\calendar')" -User $requester
        if ($?)
            {
             Write-Host "`nPermissions Removed.`n`n" -ForegroundColor green
            }
       [void](Read-Host 'Press Enter to go back to main Menu')}

    default {write-warning "Invalid Option. Please enter the correct Option #."}
}

}



