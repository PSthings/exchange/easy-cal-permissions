# Easy Cal Permissions

**menu-based options to list, edit or remove calendar permisions for single requester and owner**

---
#### Prerequisites:
**ExchangeOnlineManagement** Module <br>**`Connect-ExchangeOnline`** command must be working. We'll need that to connect to O365 ExchangeOnline.<br>Learn more: https://docs.microsoft.com/en-us/powershell/exchange/exchange-online-powershell-v2?view=exchange-ps

---
<br>
